﻿using Lab1.Kalyna;
using Lab1.AESAlgorithm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Test.txt";
            #region AES
            Console.WriteLine("AES:\n");

            AES aes = new AES();
            byte[] input = File.ReadAllBytes(filePath);

            byte[] encrypt = new byte[input.Length + 16];
            
            aes.Encrypt(input, ref encrypt);
            Console.WriteLine($"Input: {System.Text.Encoding.Default.GetString(input)}");

            byte[] decrypt = new byte[encrypt.Length];
            aes.Decrypt(encrypt, ref decrypt);
            Console.WriteLine($"Decrypted: {System.Text.Encoding.Default.GetString(input)}");
            #endregion

            Console.WriteLine("\nKALYNA:\n");
            #region KALYNA
            var f = new FileEncoderDecoder
            {
                PlainTextFileName = filePath,
                EncryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Encrypted.txt",
                DecryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Decrypted.txt",
                KeyFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Key.txt"
            };
            f.Encode();
            Console.WriteLine(f.Decode());
            #endregion
            
        }
    }
}

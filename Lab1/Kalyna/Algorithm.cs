﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Lab1.Kalyna
{
    public class Algorithm
    {
        public bool UseLog { get; set; } = false;

        private List<Block> RoundsKeys { get; } = new List<Block>();

        private static void Log(string message, Block block)
        {
            Console.WriteLine($"{message,-30} {new BigInteger(block.Data.ToArray()).ToString("X32")}");
        }

        private Block GenerateKt(Block key)
        {
            var kt = new Block
            {
                Data = new List<byte>
                {
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5
                }
            };

            kt.AddRoundKey(key);

            kt.SubBytes(StaticTables.Π);

            kt.ShiftRows();

            kt.MixColumns(StaticTables.Mds);

            kt.Xor(key);

            kt.SubBytes(StaticTables.Π);

            kt.ShiftRows();

            kt.MixColumns(StaticTables.Mds);

            kt.AddRoundKey(key);

            kt.SubBytes(StaticTables.Π);

            kt.ShiftRows();

            kt.MixColumns(StaticTables.Mds);

            return kt;
        }

        public List<Block> GenerateRoundsKeys(Block key)
        {
            for (var i = 0; i <= 10; i++)
                RoundsKeys.Add(new Block());

            var kt = GenerateKt(key);

            for (var i = 0; i <= 10; i += 2)
            {
                var roundKey = RoundsKeys[i];
                roundKey.Data = new List<byte>(StaticTables.V);
                roundKey.ShiftLeft(i / 2);

                var keyCopy = new Block(key);
                keyCopy.RotateRight(32 * i);

                roundKey.AddRoundKey(kt);
                var copy = new Block(roundKey);

                roundKey.AddRoundKey(keyCopy);

                roundKey.SubBytes(StaticTables.Π);

                roundKey.ShiftRows();

                roundKey.MixColumns(StaticTables.Mds);

                roundKey.Xor(copy);

                roundKey.SubBytes(StaticTables.Π);

                roundKey.ShiftRows();

                roundKey.MixColumns(StaticTables.Mds);

                roundKey.AddRoundKey(copy);

                RoundsKeys[i] = roundKey;
            }

            // Odd keys
            for (var i = 1; i <= 9; i += 2)
            {
                RoundsKeys[i].Data = RoundsKeys[i - 1].Data;
                RoundsKeys[i].RotateLeft(56);
            }

            return RoundsKeys;
        }

        public Block Encrypt(Block plainText, Block key)
        {
            if (RoundsKeys.Count == 0)
                GenerateRoundsKeys(key);

            var cipherText = new Block(plainText);
            cipherText.AddRoundKey(RoundsKeys[0]);

            for (var i = 1; i <= 9; i++)
            {
                cipherText.SubBytes(StaticTables.Π);

                cipherText.ShiftRows();

                cipherText.MixColumns(StaticTables.Mds);

                cipherText.Xor(RoundsKeys[i]);
            }

            cipherText.SubBytes(StaticTables.Π);

            cipherText.ShiftRows();

            cipherText.MixColumns(StaticTables.Mds);

            cipherText.AddRoundKey(RoundsKeys[10]);

            return cipherText;
        }

        public Block Decrypt(Block cipherText, Block key)
        {
            if (RoundsKeys.Count == 0)
                GenerateRoundsKeys(key);

            var plainText = new Block(cipherText);

            plainText.SubRoundKey(RoundsKeys[10]);

            plainText.MixColumns(StaticTables.MdsRev);

            plainText.ShiftRowsRev();

            plainText.SubBytes(StaticTables.ΠRev);

            for (var i = 9; 1 <= i; --i)
            {
                plainText.Xor(RoundsKeys[i]);

                plainText.MixColumns(StaticTables.MdsRev);

                plainText.ShiftRowsRev();

                plainText.SubBytes(StaticTables.ΠRev);
            }

            plainText.SubRoundKey(RoundsKeys[0]);

            return plainText;
        }
    }
}

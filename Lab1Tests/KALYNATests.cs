﻿using System;
using System.Collections.Generic;
using System.Text;
using Lab1.Kalyna;
using NUnit.Framework;
using System.IO;

namespace Lab1Tests
{
    public class KALYNATests
    {
        string filePath = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Test.txt";

        [Test]
        public void KALYNA_SuccessfullyEncrypting()
        {
            FileEncoderDecoder kalyna = new FileEncoderDecoder()
            {
                PlainTextFileName = filePath,
                EncryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Encrypted.txt",
                DecryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Decrypted.txt",
                KeyFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Key.txt"
            };

            string res = kalyna.Encode();

            Assert.NotNull(res);
        }

        [Test]
        public void KALYNA_SuccessfullyDecrypting()
        {
            FileEncoderDecoder kalyna = new FileEncoderDecoder()
            {
                PlainTextFileName = filePath,
                EncryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Encrypted.txt",
                DecryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Decrypted.txt",
                KeyFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Key.txt"
            };

            string res = kalyna.Decode();

            Assert.NotNull(res);
        }

        [Test]
        public void KALYNA_SuccessfullyEncodingAndDecryptingCheckResult()
        {
            FileEncoderDecoder kalyna = new FileEncoderDecoder()
            {
                PlainTextFileName = filePath,
                EncryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Encrypted.txt",
                DecryptedTextFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Decrypted.txt",
                KeyFileName = @"D:\Магістратура\1M\Crypto\Lab1\Lab1\Kalyna\Files\Key.txt"
            };
            string textInFile = File.ReadAllText(filePath);

            kalyna.Decode();
            string res = kalyna.Decode();

            Assert.NotNull(res);
            Assert.AreEqual(res, textInFile);
        }
    }
}

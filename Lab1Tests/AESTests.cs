using Lab1.AESAlgorithm;
using NUnit.Framework;
using System.Text;

namespace Lab1Tests
{
    public class AESTests
    {
        string textToEncrypt = "Some text))";

        [Test]
        public void AES_SuccessfullyEncryptingReturnTrue()
        {
            AES aes = new AES();
            byte[] input = Encoding.ASCII.GetBytes(textToEncrypt);
            byte[] encrypt = new byte[input.Length + 16];

            bool resultEncrypt = aes.Encrypt(input, ref encrypt);

            Assert.True(resultEncrypt);
        }

        [Test]
        public void AES_WrongInputArray()
        {
            AES aes = new AES();
            byte[] input = Encoding.ASCII.GetBytes(textToEncrypt);
            byte[] encrypt = new byte[0];

            bool resultEncrypt = aes.Encrypt(input, ref encrypt);

            Assert.False(resultEncrypt);
        }

        [Test]
        public void AES_SuccessfullyEncryptingReturnTrueAssertResult()
        {
            AES aes = new AES();
            byte[] input = Encoding.ASCII.GetBytes(textToEncrypt);
            byte[] encrypt = new byte[input.Length + 16];
            byte[] decrypt = new byte[encrypt.Length];

            bool resultEncrypt = aes.Encrypt(input, ref encrypt);
            bool resultDecrypt = aes.Decrypt(encrypt, ref decrypt);

            Assert.True(resultEncrypt);
            Assert.True(resultDecrypt);

            Assert.AreEqual(textToEncrypt, (System.Text.Encoding.Default.GetString(decrypt)).Replace("\0", string.Empty));
        }
    }
}